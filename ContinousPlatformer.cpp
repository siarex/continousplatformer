// ContinousPlatformer.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <stdio.h>
#include <tchar.h>

#include <time.h>

#include <Windows.h>	//za�o�enie, �e lecimy na windowsach

enum OPTION_INPUT
{
	EXIT = VK_ESCAPE
};

enum ACTION_INPUT		//mapowanie kod�w OSa na nasze akcje
{
	NONE = 0,
	MOVE_LEFT = VK_LEFT,
	MOVE_RIGHT = VK_RIGHT,
	JUMP = VK_SPACE
};

//static ACTION_INPUT parseInput(int input) //jedyna sluszna przerabiarka przyciskow na akcje
//{
//	
//}

struct GameState
{
	OPTION_INPUT option;
} current_GameState;

struct Point
{
	float x, y;
	Point(){ x = 0.0f; y = 0.0f; }
	Point(int in_x, int in_y) :x(static_cast<float>(in_x)), y(static_cast<float>(in_y)){}
	Point(float in_x, float in_y) :x(in_x), y(in_y){}
	Point(const Point &in){ *this = in; }
	Point& operator= (const Point &right){ this->x = right.x; this->y = right.y; return *this; }
	int getIntX() const { return static_cast<int>(x); }
	int getIntY() const  { return static_cast<int>(y); }
};

enum CollisionType
{
	WallLeft,
	WallRight
};

struct PlayerInput
{
	ACTION_INPUT aiInput;
	time_t tInterval;
};

class iPlayer
{
public:
	virtual void move(PlayerInput input){}
	virtual Point getPosition() const { return Point(); }
	virtual void collision(CollisionType collisionType){}
};

class firstPlayer : public iPlayer
{
	Point mPosition;
	Point mGround;
	Point mSpeedVector;
	const float mfArrowAcceleration = 20.0f;
	const float mfDrag = 5.0f;
	const float mfGravity = 10.0f;
public:
	firstPlayer(Point startPosition) :mPosition(startPosition) {}
	void move(PlayerInput input)
	{
		float fInterval = static_cast<float>(input.tInterval)/1000.0f; //milisekunda -> 0.001 sekundy
		if (mPosition.y > mGround.y)
		{// spadanie
			mSpeedVector.y -= (mfGravity * fInterval);
		}
		else
		{
			switch (input.aiInput)
			{
			case ACTION_INPUT::MOVE_LEFT:
				mSpeedVector.x -= (mfArrowAcceleration * fInterval);
				//tarcie pomaga
				if (mSpeedVector.x > 0.0f) mSpeedVector.x -= mfDrag * fInterval;
				break;
			case ACTION_INPUT::MOVE_RIGHT:
				mSpeedVector.x += (mfArrowAcceleration * fInterval);
				//tarcie pomaga
				if (mSpeedVector.x < 0.0f) mSpeedVector.x += mfDrag * fInterval;
				break;
			case ACTION_INPUT::JUMP:
				mSpeedVector.y += (200.0f * fInterval);;
				break;
			case ACTION_INPUT::NONE:
				//tarcie wszechmocne zatrzymaj toczenie
				if (mSpeedVector.x > 0.0f) mSpeedVector.x -= mfDrag * fInterval;
				else if (mSpeedVector.x < 0.0f) mSpeedVector.x += mfDrag * fInterval;
				break;
			}
		}
		mPosition.y += mSpeedVector.y * fInterval;
		mPosition.x += mSpeedVector.x * fInterval;
		
		if (mPosition.y < mGround.y)
		{//I HYYC O PODLOGE, PLEJER KURWA BEC
			mPosition.y = mGround.y;
			mSpeedVector.y = 0;
		}
	}
	void collision(CollisionType collisionType)
	{//bec
		switch (collisionType)
		{
		case CollisionType::WallLeft:
			mSpeedVector.x = -mSpeedVector.x / 2.0f;
			mPosition.x += 0.5f;
			break;
		case CollisionType::WallRight:
			mPosition.x -= 0.5f;
			mSpeedVector.x = -mSpeedVector.x / 2.0f;
			break;
		}
	}
	void setGround(Point ground)
	{
		mGround.y = ground.y;
	}
	Point getPosition() const { return mPosition; }
};


struct iMap
{
	//nothing ;o
};

struct FirstWorldMap : iMap
{
	Point mGround;
	Point mWallLeft;
	Point mWallRight;
	FirstWorldMap(Point stuff[]) :mGround(stuff[0]), mWallLeft(stuff[1]), mWallRight(stuff[2]){}
	FirstWorldMap(){}
};


class iRenderer
{
public:
	virtual void render(iPlayer const * player, iMap const * map){}
};

class firstRenderer : public iRenderer
{
	Point prevPos;
	void render(iPlayer const * player, iMap const * map)
	{
		FirstWorldMap const * thisMap = (FirstWorldMap const *)(map); // to jest brzydkie, ale.. jest juz ciemno, ale wszystko jedno, pytam siebie, czym jest piekno?!
		for (int h = thisMap->mGround.getIntY(); h < thisMap->mWallLeft.getIntY(); ++h)
		{
			SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), { thisMap->mWallLeft.getIntX(), 30 - h });
			printf("|");
		}
		for (int h = thisMap->mGround.getIntY(); h < thisMap->mWallRight.getIntY(); ++h)
		{
			SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), { thisMap->mWallRight.getIntX(), 30 - h });
			printf("|");
		}
		//koniec brzydoty. serio, renderujac tekst w konsoli? :D
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), { prevPos.getIntX(), 30 - prevPos.getIntY() - thisMap->mGround.getIntY() });
		printf(" ");
		Point pos = player->getPosition();
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), { pos.getIntX(), 30 - pos.getIntY() - thisMap->mGround.getIntY() });
		prevPos = pos;
		printf("X");
	}
};

class iWorld
{
protected:
	iRenderer *mRenderer;
	iPlayer *mPlayer;
	iMap *mMap;
	ACTION_INPUT mInput;
public:
	void setInput(ACTION_INPUT input)
	{
		mInput = input;
		switch (input)
		{
		case OPTION_INPUT::EXIT:
			current_GameState.option = OPTION_INPUT::EXIT;
			break;
		}
	};
	void setRenderer(iRenderer * renderer) { mRenderer = renderer; }
	void render(){ mRenderer->render(mPlayer, mMap); }
	virtual void step(long interval_ms){}
};

class firstWorld : public iWorld
{
	FirstWorldMap mFirstWorldMap;
public:
	firstWorld()
	{ 
		mPlayer = new firstPlayer(Point(5, 15)); 
		Point points[] = { Point(0.0f, 3.0f), Point(1.0f, 5.0f), Point(35.0f, 7.0f) };
		mFirstWorldMap = FirstWorldMap(points);
		mMap = (iMap *)(&mFirstWorldMap); //tez brzydkie ;o
	}
	void step(long interval_ms)
	{
		mPlayer->move({mInput, interval_ms});
		Point playerPos = mPlayer->getPosition();
		//ograniczmy sobie swiat, bo za krawedzia czai sie negatyw
		if (playerPos.x < mFirstWorldMap.mWallLeft.x) mPlayer->collision(CollisionType::WallLeft);
		else if (playerPos.x > mFirstWorldMap.mWallRight.x) mPlayer->collision(CollisionType::WallRight);
	}
};

class iInput
{
public:
	virtual ACTION_INPUT gatherInput(){ return ACTION_INPUT::NONE; }
};

class FirstInput : public iInput
{
	HANDLE hInputHandle;
	DWORD dwEvents;
	INPUT_RECORD *pirInputRecord;
	DWORD dwInputSize;
public:
	FirstInput()
	{
		/*
		* magia windowsa badzmy bogu za nia wdzieczni
		*/
		hInputHandle = GetStdHandle(STD_INPUT_HANDLE);
		dwEvents = 0;			// how many events took place
		pirInputRecord = new INPUT_RECORD();	// a record of input events
		dwInputSize = 1;		// how many characters to read
		/*
		* koniec magii
		*/
	}
	ACTION_INPUT gatherInput()
	{
		GetNumberOfConsoleInputEvents(hInputHandle, &dwEvents);
		if (dwEvents)
		{
			BOOL read = ReadConsoleInput(hInputHandle, pirInputRecord, dwInputSize, &dwEvents);
			if (read && pirInputRecord->Event.KeyEvent.bKeyDown)
				return (static_cast<ACTION_INPUT>(pirInputRecord->Event.KeyEvent.wVirtualKeyCode));
		}
		else
		{
			return (ACTION_INPUT::NONE);
		}
	}
};

int _tmain(int argc, _TCHAR* argv[])
{

	iWorld* world = new firstWorld();
	world->setRenderer(new firstRenderer());
	clock_t clockTime = clock();
	iInput* inputDevice = new FirstInput();
	while (1)
	{
		world->setInput(inputDevice->gatherInput());
		//process world
		world->step(clock() - clockTime);	//CLOCKS_PER_SEC == 1000, wiec to tutaj to bedzie czas w ms
		clockTime = clock();
		//render graphics
		world->render();
		//check conditions
		if (current_GameState.option == OPTION_INPUT::EXIT) break;
		Sleep(50);
	}
	return 0;
}

